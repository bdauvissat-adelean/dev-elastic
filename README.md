# Accessing Elasticsearch in Java

In this repository, you will find the code used in a serie of articles explaining how to request an Elasticsearch 
cluster in Java code.  

All the articles can be found on the [Adelean Blog](https://www.adelean.com/blog/)
