package com.adelean.develastic01.services.supervision;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.indices.ExistsRequest;
import co.elastic.clients.elasticsearch.indices.GetIndexRequest;
import co.elastic.clients.elasticsearch.indices.GetIndexResponse;
import co.elastic.clients.elasticsearch.indices.IndexState;
import co.elastic.clients.transport.endpoints.BooleanResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service("IndicesService")
public class Indices {

    private ElasticsearchClient elasticClient;

    public Set<String> listIndices(boolean excludeSystem) {

        GetIndexRequest request = new GetIndexRequest.Builder().index("*").build();

        try {
            GetIndexResponse response = elasticClient.indices().get(request);
            Map<String, IndexState> indices = response.result();

            if (excludeSystem) {
                return indices.keySet().stream().filter(s -> !s.startsWith(".")).collect(Collectors.toSet());
            }

            return indices.keySet();

        } catch (IOException e) {

            throw new RuntimeException(e);
        }

    }

    public IndexState indiceDetail(String indice) {

        try {

            ExistsRequest existsRequest = new ExistsRequest.Builder().index(indice).build();

            BooleanResponse existResponse = elasticClient.indices().exists(existsRequest);

            if (!existResponse.value()) {
                return null;
            }

            GetIndexRequest request = new GetIndexRequest.Builder().index(indice).build();
            GetIndexResponse response = elasticClient.indices().get(request);
            Map<String, IndexState> indices = response.result();

            return indices.get(indice);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Autowired
    public void setElasticClient(ElasticsearchClient elasticClient) {
        this.elasticClient = elasticClient;
    }
}
