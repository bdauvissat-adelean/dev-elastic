package com.adelean.develastic01.services.search;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class Product {

    private ElasticsearchClient elasticClient;

    public long findNumberOfProducts(String index, String product) {

        SearchRequest request = new SearchRequest.Builder()
                .index(index)
                .query(q -> q.match(m -> m.field("products.product_name").query(product))).build();

        try {
            SearchResponse<ObjectNode> response = elasticClient.search(request, ObjectNode.class);
            return response.hits().total().value();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    @Autowired
    public void setElasticClient(ElasticsearchClient elasticClient) {
        this.elasticClient = elasticClient;
    }
}
