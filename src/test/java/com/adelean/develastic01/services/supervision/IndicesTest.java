package com.adelean.develastic01.services.supervision;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class IndicesTest {

    @Autowired
    private Indices indicesService;

    @Test
    void listAllIndices() {

        var result = indicesService.listIndices(false);

        assertEquals(12, result.size());

    }

    @Test
    void listNonSystemIndices() {

        var result = indicesService.listIndices(true);

        assertEquals(1, result.size());

    }

    @Test
    void indiceDetailExistingIndice() {

        var result = indicesService.indiceDetail("kibana_sample_data_ecommerce");
        assertEquals("1", result.settings().index().numberOfReplicas());

    }

    @Test
    void indiceDetailNonExistingIndice() {

        var result = indicesService.indiceDetail("this_indice_does_not_exist");
        assertNull(result);

    }
}
