package com.adelean.develastic01.services.search;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ProductTest {

    @Autowired
    Product product;

    @Test
    void findProduct() {

        product.findNumberOfProducts("kibana_sample_data_ecommerce", "trousers");

    }
}
